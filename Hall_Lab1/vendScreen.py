'''
@file vendScreen.py
@brief This file contains classes to model the operation and screen output for Vendotron.

@detail Within this file, two classes are defined to allow for the operation
of the Vendotron virtual vending machine. The first class vendBalance. 
This class has two components - the current balance (labeled bal), and the 
change denominations. This class contains two methods - one to increase balance 
corresponding to a currency input and one to compute remaining balance and 
change denominations given a product price. The second class is vendScreen. 
This class models an LCD display typical of vending machines or similar 
devices. It has a fixed portion that is always displayed, three 40 character 
display lines, and a debug window to display the mechanical functions of 
Vendotron. To model the display, this class prints a number of empty lines 
each time it is called, to give the impression of a continuously updated monitor. 

@package Vendotron
Module containing the two required external classes for the Vendotron virtual vending machine.

Two classes are contained in this module. The first class vendBalance. 
This class has two components - the current balance (labeled bal), and the 
change denominations. This class contains two methods - one to increase balance 
corresponding to a currency input and one to compute remaining balance and 
change denominations given a product price. The second class is vendScreen. 
This class models an LCD display typical of vending machines or similar 
devices. It has a fixed portion that is always displayed, three 40 character 
display lines, and a debug window to display the mechanical functions of 
Vendotron. To model the display, this class prints a number of empty lines 
each time it is called, to give the impression of a continuously updated monitor. 

@author Rick Hall

@date Tue Jan 19 16:58:16 2021
'''

class vendBalance:
    '''
    @brief This class stores data and conducts operations relevant to the balance of currency for Vendotron. 
    @details This class stores the balance and supplied denominations of currency
    inserted in the Vendotron machine. It includes methods for adding to the 
    current ballance by giving a coin input (related to the Vendotron pay
    tuple) and determining appropriate change to be returned given a product
    price. It both computes the change numerically and develops a tuple of
    optimal change denominations to be returned.
    '''
    
    def __init__(self,bal, denom):
        '''
        @brief Initialization of the vendBalance variable
        @details This variable stores a numerical balance of currency (in cents)
        and a tuple containing the denominations that make up that balance.
        @param bal The current balance
        @param denom The denomination tuple
        '''
        
        ## The Current Balance, in Cents
        self.bal = bal
        
        ## The denominations from which the balance is composed
        self.denom = denom

    def getBal(self,coin):
        '''
        @brief This function increases the balance based upon a coin input.
        @details Given a coin input (corresponding to an index in the Vendotron
        payment tuple, from 0 to 7), this function adds the appropriate value,
        in cents, to the current balance. It has no return
        @param coin The index of the appropriate coin denomination
        ''' 
        
        pay_indx = (1,5,10,25,100,500,1000,2000) 
        n = 7
        
        while n >= 0:
            if coin == n:
                self.bal = self.bal + pay_indx[n]
                den = list(self.denom)
                den[n] = den[n] + 1
                self.denom = tuple(den)
            n = n-1
    
    def getChg(self,price):
        '''
        @brief This function computes the change of a purchace given a product price. 
        @details When given a product price, this function computes both the
        numerical change to be returned, in cents, and a tuple containing the
        optimal return denominations. It updates the balance and denominations
        of the input variable accordingly, and returns a boolean indicating if
        the balance was sufficient to cover the price. If the balance is 
        insufficient, the balance is not changed.
        @param price The price of the object being purchased
        @return sufficient Boolean indicating if the balance was greater than the cost
        '''
        pay_indx = (1,5,10,25,100,500,1000,2000)    # Defines value of each location in payment/change lists
        
        chng_out = [0,0,0,0,0,0,0,0]    # Define change list
            
        # From decimal payment, determine change ammount
        chng = self.bal - price
        chng_in = chng
        
        # Determine if payment was sufficient and if change should be returned
        if chng_in > 0:
            chng_state = 1     # Payment exceeded price - Compute change
        elif chng_in == 0:
            chng_state = 2     # Payment exactly matched price - Skip computation
        else:
            chng_state = 3     # Payment insufficient - Return zero tuple and Error
    
       
        # Run change computation
        if chng_state == 1:
            pointer = 7    # len(pay_indx) - 1    # Reset pointer to max index of pay_inx. Preallocated for speed.
            while pointer >= 0:     # Repeat until pointer has run through each location of payment list
                while chng_in >= pay_indx[pointer]:     # Repeat until 
                    chng_in = chng_in - pay_indx[pointer]        # For each loop: Decrease change numeral by ammount at pointer location of pay_indx
                    chng_out[pointer] = chng_out[pointer] + 1   #                 Increment location of change list
                    
                pointer = pointer - 1   # Decrement pointer
        
        # Give Return Boolean for sufficient funding
        if chng_state == 3:
            sufficient = 0
        else:
            sufficient = 1
            self.bal = chng
            self.denom = tuple(chng_out)
                
        return sufficient



class vendScreen:
    '''
    @brief This class is a virtual LCD screen for Vendotron, displayed in the Python command window.  
    @details This class models an LCD display typical of vending machines or 
    similar devices. It has a fixed portion that is always displayed, three 40 
    character display lines, and a debug window to display the mechanical 
    functions of Vendotron. To model the display, this class prints a number 
    of empty lines each time it is called, to give the impression of a 
    continuously updated monitor.
    @author Rick Hall
    '''
    
    def __init__(self,ln1,ln2,ln3,dbg):
        '''
        @brief Initialization of the vendScreen variable
        @details This function initializes the vendScreen variable and allows
        for defining the individual lines of the screen. With the exception of
        the debug line (which is purely a method to view the mechanical 
        operations of the Vendotron), the lines are truncated to the first
        fourty digits to correspond to the available slots in the display.
        @param ln1 The first line of the display   
        @param ln2 The second line of the display
        @param ln3 The third line of the display 
        @param dbg The debug line on the display
        '''
        
        ## The first line of the display
        self.ln1 = ln1[:40]
        
        ## The second line of the display
        self.ln2 = ln2[:40]
        
        ## The third line of the display
        self.ln3 = ln3[:40]
        
        ## The debug line of the display
        self.dbg = dbg
        

    def Prnt(self):
        '''
        @brief Prints the Vendotron screen with the current state.
        @details This function takes the vendScreen outputs the Vendotron 
        display with the current lines stored in the indicated vendScreen
        variable.
        '''
        print('\n'*30)
        print('----- VENDOTRON ----------------------------------'
              + '\n'*2 + "     " + self.ln1
              + '\n'*2 + "     " + self.ln2
              + '\n'*3 + "     " + self.ln3
              + '\n'*2 +
              '--------------------------------------------------'
              + '\n' + 'DEBUG: ' + self.dbg + '\n' +
              '--------------------------------------------------')
              
              
              
