'''
@file vendBalance.py
@brief This file contains a class to support the balance computations for Vendotron

@details Within this file, the class vendBalance is defined. This class has 
two components - the current balance (labeled bal), and the change denominations. 
This class contains two methods - one to increase balance corresponding to a 
currency input and one to compute remaining balance and change denominations 
given a product price. 

@author Rick Hall
@date Thu Jan 14 09:50:49 2021
'''

class vendBalance:
    '''
    @brief This class stores data and conducts operations relevant to the balance of currency for Vendotron. 
    @details This class stores the balance and supplied denominations of currency
    inserted in the Vendotron machine. It includes methods for adding to the 
    current ballance by giving a coin input (related to the Vendotron pay
    tuple) and determining appropriate change to be returned given a product
    price. It both computes the change numerically and develops a tuple of
    optimal change denominations to be returned.
    '''
    
    def __init__(self,bal, denom):
        '''
        @brief Initialization of the vendBalance variable
        @details This variable stores a numerical balance of currency (in cents)
        and a tuple containing the denominations that make up that balance.
        @param bal The current balance
        @param denom The denomination tuple
        '''
        
        ## The Current Balance, in Cents
        self.bal = bal
        
        ## The denominations from which the balance is composed
        self.denom = denom

    def getBal(self,coin):
        '''
        @brief This function increases the balance based upon a coin input.
        @details Given a coin input (corresponding to an index in the Vendotron
        payment tuple, from 0 to 7), this function adds the appropriate value,
        in cents, to the current balance. It has no return
        @param coin The index of the appropriate coin denomination
        ''' 
        
        pay_indx = (1,5,10,25,100,500,1000,2000) 
        n = 7
        
        while n >= 0:
            if coin == n:
                self.bal = self.bal + pay_indx[n]
                den = list(self.denom)
                den[n] = den[n] + 1
                self.denom = tuple(den)
            n = n-1
    
    def getChg(self,price):
        '''
        @brief This function computes the change of a purchace given a product price. 
        @details When given a product price, this function computes both the
        numerical change to be returned, in cents, and a tuple containing the
        optimal return denominations. It updates the balance and denominations
        of the input variable accordingly, and returns a boolean indicating if
        the balance was sufficient to cover the price. If the balance is 
        insufficient, the balance is not changed.
        @param price The price of the object being purchased
        @return sufficient Boolean indicating if the balance was greater than the cost
        '''
        pay_indx = (1,5,10,25,100,500,1000,2000)    # Defines value of each location in payment/change lists
        
        chng_out = [0,0,0,0,0,0,0,0]    # Define change list
            
        # From decimal payment, determine change ammount
        chng = self.bal - price
        chng_in = chng
        
        # Determine if payment was sufficient and if change should be returned
        if chng_in > 0:
            chng_state = 1     # Payment exceeded price - Compute change
        elif chng_in == 0:
            chng_state = 2     # Payment exactly matched price - Skip computation
        else:
            chng_state = 3     # Payment insufficient - Return zero tuple and Error
    
       
        # Run change computation
        if chng_state == 1:
            pointer = 7    # len(pay_indx) - 1    # Reset pointer to max index of pay_inx. Preallocated for speed.
            while pointer >= 0:     # Repeat until pointer has run through each location of payment list
                while chng_in >= pay_indx[pointer]:     # Repeat until 
                    chng_in = chng_in - pay_indx[pointer]        # For each loop: Decrease change numeral by ammount at pointer location of pay_indx
                    chng_out[pointer] = chng_out[pointer] + 1   #                 Increment location of change list
                    
                pointer = pointer - 1   # Decrement pointer
        
        # Give Return Boolean for sufficient funding
        if chng_state == 3:
            sufficient = 0
        else:
            sufficient = 1
            self.bal = chng
            self.denom = tuple(chng_out)
                
        return sufficient
