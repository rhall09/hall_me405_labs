'''
@file ME405-Lab0x01-RHall.py
@brief This file contains a Finite State Machine (FSM) designed to operate a hypothetical vending machine "Vendotron."

@details This file contains a Finite State Machine (FSM) to operate a hypothetical 
vending machine "Vendotron." To model the machine, a virtual LCD display is modeled 
in the command window with a debug window to display the mechanical operations 
of the machine.

@date Thu Jan 14 08:23:38 2021

@author Rick Hall
'''


state = 0 # Set initial state to 0 to ensure proper initialization

#<<<<<>>>>>-----------------Run Finite State Machine-----------------<<<<<>>>>>
while True:
    
    #<<<<<-----State 0 - Initialization----------------------------------->>>>>
    if state == 0:
        '''
        State 0 - System Initialization. In this state, all necessary external
        modules are imported, all variables are initialized, and external
        hardware (buttons and screen) are set up to be modeled. The buttons are
        modeled with keyboard inputs, and the screen is modeled with a
        standardized screen output established in the 'VendScreen' class.
        '''
        # Load External Functions
        from Vendotron import vendBalance # Load balance class
        from Vendotron import vendScreen  # Load screen class
        # Initialize Keyboard
        import keyboard 
        # Initialize Timer
        import time

        #-----Initialize Keyboard Operation------------------------------------
        last_key = ' '
        keyPress = 0

        def kb_cb(key):
            '''
            @brief Callback function which is called when a key has been pressed.
            @param key The keyboard key that has been pressed
            '''
            
            global last_key
            global keyPress
            
            # Store key value for use in Script
            last_key = key.name
            
            # Determine Keypress type & store in type indicator
            if key.name == "c" or key.name == "p" or key.name == "s" or key.name =="d":
                keyPress = 1    # If keypress corresponds to product button
            elif key.name == "0" or key.name == "1" or key.name == "2" or key.name == "3" or key.name == "4" or key.name == "5" or key.name == "6" or key.name == "7":
                keyPress = 2    # If keypress corresponds to coin input
            elif key.name == "e":
                keyPress = 3    # If keypress corresponds to eject button
            else:
                keyPress = 0    # If keypress is not expected value, ignore
            
        # Tell the keyboard module to respond to key release
        keyboard.on_release(callback=kb_cb)
        
        # Defunct section - should respond only to specified keys, in effect works as above.
        #keyboard.on_release_key("q", callback=kb_cb)
        #keyboard.on_release_key("w", callback=kb_cb)
        #keyboard.on_release_key("e", callback=kb_cb)
        #keyboard.on_release_key("r", callback=kb_cb)
        #keyboard.on_release_key("t", callback=kb_cb)
        
        #keyboard.on_release_key("1", callback=kb_cb)
        #keyboard.on_release_key("2", callback=kb_cb)
        #keyboard.on_release_key("3", callback=kb_cb)
        #keyboard.on_release_key("4", callback=kb_cb)
        #keyboard.on_release_key("5", callback=kb_cb)
        #keyboard.on_release_key("6", callback=kb_cb)
        #keyboard.on_release_key("7", callback=kb_cb)
        #keyboard.on_release_key("8", callback=kb_cb)
        
        #-----Initialize General Variables-------------------------------------
        pay = 0
        chng = 0
        bal = vendBalance(0,(0,0,0,0,0,0,0,0))
        prod = ' '
        coin = ' '
        balUpd = 0
        dspUpd = 0
        vendProd = 0
        ejcChg = 0
        chgOut = 0
        cPause = 0
        tick = 0
        tock = 0
        IdleTime = 0
        IdleMsg  = 0
        
        # Define Price for Each Item
        press = ("c","p","s","d")
        product = ("Cuke","Popsi","Spryte","Dr. Pupper")
        price = (100,120,85,110)
        
        # Define Payment String Tuple for Display
        payTups = ("penny","nickel","dime","quarter","dollar","five","ten","twenty")
        payTupp = ("pennies","nickels","dimes","quarters","ones","fives","tens","twenties")
        
        #-----Initialize Display Variables-------------------------------------
        dIdle = 0
        dStby = 0
        dBal = 0
        dPay = 0
        dVend = 0
        dIns = 0
        dChng = 0
        dWait = 0
        dPause = 0
        dOrd = 0
        IdleTick = 0
        
        #-----Initialize Screen------------------------------------------------
        scrn = vendScreen(' ',' ',' ',' ')
        dIdle = 1
        dspUpd = 1        

        # Reset to State 1
        state = 1
        #-------------------->>>>> Exit State
    
    
    #<<<<<-----State 1 - Hub---------------------------------------------->>>>>
    elif state == 1:
        '''
        State 1 - FSM Hub State. In this state, the next state of the FSM is
        determined. Using a series of if/elif/else statements, the priority of
        states is determined and the state is set according to active booleans.
        Additionally, this state operates a timer to standardize the loop time
        and allow for specific cooperative time delays in other states.
        '''
        time.sleep(0.001)
        tick = tick + 1
        if tick == 1000:
            cPause = 1
            dPause = 1
            tock = tock + 1
            tick = 0
            if dStby == 0 and dOrd == 0:
                dPay = 1
                dspUpd = 1
            if tock == 15:
                dIdle = 1
                dspUpd = 1
                tock = 0
            
        if keyPress != 0:
            state = 2
        elif balUpd == 1:
            state = 3
        elif dspUpd == 1:
            state = 4
        elif vendProd == 1:
            state = 5
        elif ejcChg == 1:
            state = 6
        else:
            if dPause == 1:
                IdleTime = IdleTime + 1
                dPause = 0
    
    #<<<<<-----State 2 - Input Processing--------------------------------->>>>>
    elif state == 2:
        '''
        State 2 - Input Processing. This state is called any time the keyPress
        variable is not set to 0. In this state, the value of keyPress is
        used to filter the type of keypress, then a series of if/else 
        statements determines the exact key that was pressed and stores it as 
        a variable. The keypress is then used to raise the neccessary boolean 
        flags for appropriate following states.
        '''
        # If key press corresponds to a drink, determine what drink
        if keyPress == 0:
            scrn.dbg = 'Keyboard Error 0'
            scrn.Prnt()
        elif keyPress == 1:
            if last_key == press[0]:
                prod = 0        # Cuke Button
            elif last_key == press[1]:
                prod = 1        # Popsi Button
            elif last_key == press[2]:
                prod = 2        # Spryte Button
            elif last_key == press[3]:
                prod = 3        # Dr. Pupper Button
            else:
                print('Keyboard Error 1')
            
            keyPress = 0
            scrn.dbg = "<> The " + product[prod] + " button has been pressed <>"
            scrn.Prnt()
            balUpd = 1  # Raise Balance Update Flag
            dPay = 1
        
        # If key press corresponds to a coin input, determine what coin    
        elif keyPress == 2:
            if last_key == '0':
                coin = 0        # Penny Inserted
            elif last_key == '1':
                coin = 1        # Nickel Inserted
            elif last_key == '2':
                coin = 2        # Dime Inserted
            elif last_key == '3':
                coin = 3        # Quarter Inserted
            elif last_key == '4':
                coin = 4        # Dollar Inserted
            elif last_key == '5':
                coin = 5        # Five Inserted
            elif last_key == '6':
                coin = 6        # Ten Inserted
            elif last_key == '7':
                coin = 7        # Twenty Inserted
            else:
                scrn.dbg = 'Keyboard Error 2'
                scrn.Prnt()
                
            scrn.dbg = ("<> You have inserted a " + payTups[coin] + " <>")
            scrn.Prnt()
            keyPress = 0
            balUpd = 1  # Raise Balance Update Flag
            dPay = 1
            
        elif keyPress == 3:
            ejcChg = 1
            dOrd = 0
            dPause = 0
            cPause = 0
            keyPress = 0

        else:
            scrn.dbg = 'Keyboard Error 3' 
            scrn.Prnt()
        
        dIdle = 0   
        tock = 0
        
        # Reset to State 1
        state = 1
        #-------------------->>>>> Exit State
    
    #<<<<<-----State 3 - Balance Computation------------------------------>>>>>
    elif state == 3:
        '''
        State 3 - Balance Computation. This state is called any time the
        current balance of payment needs to be updated. In this state, the
        user's current balance is tracked in a variable of the 'balance' 
        class, with appropriate methods for both coin insertion and drink
        selection. This state also computes appropriate change upon drink
        selection. 
        '''
        
        # If a coin has been entered:
        if coin != ' ':
            bal.getBal(coin)    # Add the coin value to the current balance
            coin = ' '
            dBal = 1
            dspUpd = 1
            balUpd = 0
        # If a product has been entered:
        elif prod != ' ':
            suf = bal.getChg(price[prod])   # Check if balance sufficient & compute change
            
            # Set display to message if payment insufficient
            if suf == 0:
                dIns = 1
                dspUpd = 1
            # Vend Soda if Payment Sufficient
            else:
                vendProd = 1
                dVend = 1
                dBal = 1
                dspUpd = 1
            balUpd = 0
        else:         
            scrn.dbg("Balance Error 1")
            scrn.Prnt()
            balUpd = 0
        
        # Reset to State 1
        state = 1
        #-------------------->>>>> Exit State
    
    
    #<<<<<-----State 4 - Display------------------------------------------>>>>>
    elif state == 4:
        '''
        State 4 - Display Driver. This state is called any time the display
        needs to be updated. This state uses a variable of the 'VendScreen'
        class to track the display values in a modeled display. The display
        model has a fixed format with three 40 character lines, each of which
        is set as necessary. Additionally, the screen contains a debug line,
        which is the only line updated outside of this state. The debug line
        is utilized to debug the functions of the FSM as each state occurs, and
        primarily represents the actions of modeled hardware (buttons &
        dispensers).
        '''
        # For any given display boolean, display the appropriate message.
        # For each state except the standby screen and the idle screen, reset standby delay.
        if dWait == 1:
            if dPause == 1:
                dWait = 0
        elif dBal == 1:
            val = format((bal.bal/100),'.2f')
            scrn.ln3 = "Current balance:    $" + val
            
            dBal = 0
            scrn.Prnt()
        elif dPay == 1:
            scrn.ln1 = "Please insert payment or make a selection"
            scrn.ln2 = ' '
            
            dStby = 1
            dPay = 0
            scrn.Prnt()
        elif dIns == 1:
            if bal.bal != 0:
                scrn.ln1 = "Payment insuficient:"
                dBal = 1
            else:
                scrn.ln1 = "Please insert payment:"
            val = format((price[prod]/100),'.2f')
            scrn.ln2 = product[prod] + " costs $" + val
            prod = ' '
            
            dStby = 0
            dPause = 0
            tick = 0
            dIns = 0
            scrn.Prnt()
        elif dVend == 1:
            scrn.ln1 = "Enjoy Your " + product[prod] + "!"
            scrn.ln2 = ' '
            
            dWait = 1
            dStby = 0
            dPause = 0
            tick = 0
            dVend = 0
            scrn.Prnt()
        elif dChng == 1:
            val = format((chng/100), '.2f')
            scrn.ln1 = "Your change is $" + val
            scrn.ln2 = "Please come again!"
            scrn.ln3 = "Dispensing: " + "-"*chgOut + "[]" + "-"*(9-chgOut)
            
            dStby = 0
            dPause = 0
            scrn.Prnt()
            dspUpd = 0
        elif dIdle == 1:
            if IdleTick == 0:
                scrn.ln1 = "Welcome to Vendotron!"
                if bal.bal == 0:
                    scrn.ln3 = ' '
                IdleTick = IdleTick + 1
                dPause = 0
                scrn.Prnt()
            elif dPause == 1 and IdleTick != (len(price) + 1):
                x = IdleTick - 1
                scrn.ln1 = "Our selection includes:"
                scrn.ln2 = product[x] + ":   $" + format(price[x]/100, '.2f') + "    (Press " + press[x] + ")"
                IdleTick = IdleTick + 1
                dPause = 0
                scrn.Prnt()
            elif dPause == 1:
                dBal = 1
                dPay = 1
                dIdle = 0
                IdleTick = 0
                dPause = 0
                dStby = 0
        else:
            IdleTick = 0
            dspUpd = 0
        
        # Reset to State 1
        state = 1
        #-------------------->>>>> Exit State
    
    
    #<<<<<-----State 5 - Dispense Soda------------------------------------>>>>>
    elif state == 5:
        '''
        State 5 - Soda Dispenser. This state models the hardware to dispense
        the selected brand of soda. As no physical hardware exists, this state
        simply prints a line to the debug section of the virtual display. This
        state also determines if the user should be prompted for another
        selection or given change.
        '''        
        # Print Debug Message to Indicate Soda Dispense
        scrn.dbg = "<> " + "One " + product[prod] + " dispensed" + " <>"
        scrn.Prnt()
        vendProd = 0    # Clear Vend Bool
        
        # Determine if change should be dispensed
        if bal.bal >= min(price):   # If another soda could be purchased:
            dPay = 1
            dspUpd = 1
        else:                       # If no additional soda could be purchased:
            dChng = 1
            ejcChg = 1
        IdleMsg = 0
            
        # Reset to State 1
        state = 1
        #-------------------->>>>> Exit State
    
    #<<<<<-----State 6 - Dispense Change---------------------------------->>>>>
    elif state == 6:
        '''
        State 6 - Change Dispenser. This state models the hardware that 
        dispenses appropriate change denominations to the user. As no physical 
        hardware exists, this state simply prints a line to the debug section 
        of the virtual display.
        '''
        
        tock = 0
        
        if chgOut == 0:
            bal.getChg(0)           #Recompute change to optimize output
            chng = bal.bal
            chngTup = bal.denom
            bal.bal = 0
            bal.denom = (0,0,0,0,0,0,0,0)
            dChng = 1
            dspUpd = 1
            cPause = 0
            chgOut = 1
        if chgOut <= len(payTups) + 2 and cPause == 1:  
            x = chgOut - 1
            if chngTup[x] > 0:
                if chngTup[x] == 1:
                    scrn.dbg = "<> One " + payTups[x] + " has been dispensed <>"
                else:
                    scrn.dbg = "<> " + str(chngTup[x]) + " " + payTupp[x] + " have been dispensed" + " <>"
                scrn.Prnt()
            chgOut = chgOut + 1
            cPause = 0
            dspUpd = 1
        elif chgOut == len(payTups) + 1:
            ejcChg = 0
            chgOut = 0
            chng = 0
            chngTup = 0
            dBal = 1
            dChng = 0
            dspUpd = 1
            cPause = 0
        
        # Reset to State 1
        state = 1
        #-------------------->>>>> Exit State