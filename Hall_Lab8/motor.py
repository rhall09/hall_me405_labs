# -*- coding: utf-8 -*-
"""
Created on Tue Mar  9 09:46:10 2021

@author: hallm
"""

import pyb
import utime

class MotorDriver:
    '''
    
    '''
    
    def __init__ (self, nSLEEP_pin, Channel_1, IN1_pin, Channel_2, IN2_pin, timer, nFAULT_pin):
        '''
        
        
        '''
        
        self.nSLEEP = pyb.Pin(nSLEEP_pin,mode=pyb.Pin.OUT_PP)
        self.nFAULT = nFAULT_pin
        self.Fault = False
        self.Tmr = timer
        self.IN1 = self.Tmr.channel(Channel_1,pyb.Timer.PWM,pin=IN1_pin)
        self.IN2 = self.Tmr.channel(Channel_2,pyb.Timer.PWM,pin=IN2_pin)
        
        
        
    def enable(self):
        '''
        
        '''
        
        self.nSLEEP.high()
        
    def disable(self):
        '''
        
        '''
        
        self.nSLEEP.low()
        
        
        
    def set_duty(self,duty_cycle):
        '''
        '''
        if self.Fault == False:        
            if duty_cycle == 0:
                self.IN1.pulse_width_percent(0)
                self.IN2.pulse_width_percent(0)
            elif duty_cycle > 0:
                self.IN1.pulse_width_percent(abs(duty_cycle))
                self.IN2.pulse_width_percent(0)  
            elif duty_cycle < 0:
                self.IN1.pulse_width_percent(0)
                self.IN2.pulse_width_percent(abs(duty_cycle)) 
            
            
if __name__ == '__main__':
    
    mTim = pyb.Timer(3, freq=120, period=0xFFFF)
    xMot = MotorDriver(pyb.Pin.board.PA15, 1, pyb.Pin.board.PB4, 2, pyb.Pin.board.PB5, mTim, pyb.Pin.board.PB2)
    yMot = MotorDriver(pyb.Pin.board.PA15, 3, pyb.Pin.board.PB0, 4, pyb.Pin.board.PB1, mTim, pyb.Pin.board.PB2)
    
    xMot.enable()
    yMot.enable()
    
    while True:
        try:
            xSpeed = input('Designate X Duty Cycle:     >>>     ')
            xSpeed = int(xSpeed)
            ySpeed = input('Designate Y Duty Cycle:     >>>     ')
            ySpeed = int(ySpeed)
            
            xMot.set_duty(xSpeed)
            yMot.set_duty(ySpeed)
            
            while True:
                try:
                    utime.sleep_ms(1000)
                except KeyboardInterrupt:
                    xMot.set_duty(0)
                    yMot.set_duty(0)
                    break
        except KeyboardInterrupt:
            xMot.disable()
            yMot.disable()
            break