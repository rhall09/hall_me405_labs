# -*- coding: utf-8 -*-
"""
Created on Tue Mar  9 19:45:10 2021

@author: hallm
"""

import pyb
from MotorDriver import MotorDriver
from EncoderDriver import EncoderDriver


xEnc = EncoderDriver('PB6','PB7',4)
yEnc = EncoderDriver('PC6','PC7',8)


# Create the pin objects used for interfacing with the motor driver
pin_nSLEEP = 'A15'
pin_IN1 = 'B4'
pin_IN2 = 'B5'
pin_IN3 = 'B0'
pin_IN4 = 'B1'

# Create the timer objects used for PWM generation
tim = 3
    
xMot = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, tim, 1)
# yMot = MotorDriver(pin_nSLEEP, pin_IN3, pin_IN4, tim, 2)
        
xMot.enable()
# yMot.enable()   

P = 1

I = 0

fault = 0

xIsum = 0
# yIsum = 0

line = 'Ypos:   {a:.1f}     Xpos:   {b:.1f}     Condition:     '

def fault_check(line):
    '''
    @brief This function calls when the nFault pin, pin B2, is low, indicating an overcurrrent condition in the motor.
    @param Line This param allows for functionality of the interrupt service request.
        '''
    # this code disables both motors and turns the fault variable to 1, 
    # triggering the motor fault user interface
    global fault
    xMot.disable()
    # yMot.disable()
    fault = 1
        
# Define external interupt to stop motors if there is a fault. The 
# nFAULT pin, pin B2 on the motordriver chip, automatically detects motor 
# faults (overcurrent conditions). It is active low, meaning that a low 
# signal indicates a fault. 
extint = pyb.ExtInt(pyb.Pin(pyb.Pin.board.PB2, mode=pyb.Pin.IN),
                    pyb.ExtInt.IRQ_FALLING, 
                    pyb.Pin.PULL_NONE, 
                    callback = fault_check)
    
while True:
    try:
        input('Press enter to enable controller:     \n')
        P = float(input('Enter Proportional gain:     >>>   '))
        I = float(input('Enter Integral gain:     >>>   '))
        while True:
            xEnc.update()
            yEnc.update()
            
            xErr = yEnc.deg_pos - xEnc.deg_pos
            xIsum = xIsum + xErr
            
            xSig = P*xErr + I*xIsum
            
            if xSig > 100:
                xSig = 100
                cond = 'Saturated Positive'
            elif xSig < -100:
                xSig = -100
                cond = 'Saturated Negative'
            else:
                cond = 'Stable'
                
            if fault == 0:
                xMot.set_duty(xSig)
            else:
                while True:
                    fault = input('Fault Detected, type "Reset" to continue:     >>>   ')
                    if fault == 'Reset' or fault == 'reset':
                        xMot.enable()
                        fault = 0
                        break
                    
                    
                
            
            print('\n'*50 + line.format(a=yEnc.deg_pos, b=xEnc.deg_pos) + cond)
        
        
        
    except KeyboardInterrupt:
        break