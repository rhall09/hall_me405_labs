# -*- coding: utf-8 -*-
"""

@author Rick Hall
@date Thu Mar  4 08:57:27 2021
"""

from pyb import Timer as Tmr
import array
import utime

class EncoderDriver:
    '''
    @brief Encoder module for Balancing Table Lab ME 405
    
    This class reads from a two-chanel encoder...
    
    '''
    
    def __init__(self,Encoder_Timer):
        '''
        @brief Initialization function for encoder
        
        Intializes the encoder with pins defined by the two channel inputs. 

        
        @param CH1 The first channel of the encoder. Enter as a string - ex: 'PB6'
        @param CH2 The second channel of the encoder. Enter as a string - ex: 'PB6'

        '''
        
        self.tim = Tmr(Encoder_Timer,period=0xFFFF,prescaler=0)
        self.tc1 = self.tim.channel(1,Tmr.ENC_AB)
        self.tc2 = self.tim.channel(2,Tmr.ENC_AB)
        
        self.rPos = array.array('I', [0, 0, 0])
        self.rDta = array.array('l', [0, 0, 0])
        self.dPos = 0
        self.dDta = 0
        
    def update(self):
        '''
        @brief Reads the encoder position - Optimally read on regular interval
        
        Detailed description .................

        '''
        
        self.rPos[0] = self.rPos[1]
        self.rPos[1] = self.rPos[2]
        self.rPos[2] = self.tim.counter()
        
        self.rDta[0] = self.rDta[1]
        self.rDta[1] = self.rDta[2]
        
        self.rDta[2] = self.rPos[-1] - self.rPos[-2]
        
        
        if -0x7FFF < self.rDta[-1] < 0x7FFF:
            pass
        elif self.rDta[2] < -0x7FFF:
            self.rDta[2] = self.rDta[-1] + 0xFFFF
        elif self.rDta[2] > 0x7FFF:
            self.rDta[2] = self.rDta[-1] - 0xFFFF
        else:
            pass    
        
        self.dDta = 360 * (self.rDta[-1]/4000)
        self.dPos = self.dPos + self.dDta
        
        
    def get_position(self):
        '''
        @brief Returns the most recent encoder position
        
        Details?
        
        '''
        
        return self.dPos
    
    
    def set_position(self,Position):
        '''
        @brief Sets the current encoder position to a user-defined position
        
        Details?
        
        @param Position - The manually entered position of the encoder        
        '''
        
        self.dPos = Position
        
    def get_delta(self):
        '''
        @brief Computes change in position based on encoder reading
        
        Details?


        '''
        
        self.dlta = self.rPos[-1] - self.rPos[-2]
        
        
        
    
    
    
if __name__ == '__main__':
    encx = EncoderDriver(4)
    ency = EncoderDriver(8)
    
    while True:
        try:
            t1 = utime.ticks_us()
            encx.update()
            ency.update()
            t2 = utime.ticks_us()
            readtime = utime.ticks_diff(t2,t1)
            
            line = 'Encoder X: {px:.2f} --> {dx:.2f}     Encoder Y: {py:.2f} --> {dy:.2f}'
            
            print('\n'*50)
            print(line.format(px=encx.rPos[-1],dx=encx.rDta[-1],py=ency.rPos[-1],dy=ency.rDta[-1]))
            print(line.format(px=encx.dPos,dx=encx.dDta,py=ency.dPos,dy=ency.dDta))
            print('Readtime: ' + str(readtime))
            utime.sleep_ms(500)
            
        except KeyboardInterrupt:
            break
    