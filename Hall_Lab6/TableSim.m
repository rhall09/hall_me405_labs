function dX = TableSim(X,T)
%State Space Simulation of Tilted Table

%% Constant Definitions

%Constant                 [Units]               [Description]
r_m = 60;                   %mm                 [Radius of Lever Arm]
l_r = 50;                   %mm                 [Length of Push Rod]
r_b = 10.5;                 %mm                 [Radius of Ball]
r_g = 42;                   %mm                 [Vertical Distance from U-Joint to CG of Platform]
l_p = 110;                  %mm                 [Horizontal Distance from U-Joint to Push-Rod Pivot]
r_p = 32.5;                 %mm                 [Vertical Distance from U-Joint to Push-Rod Pivot]
r_c = 50;                   %mm                 [Vertical Distance from U-Joint to Platfrom Service]
m_b = 30;                   %g                  [Mass of Ball]
m_p = 400;                  %g                  [Mass of Platform]
I_p = 1.88E6;               %g mm^2             [Moment of Inertia of Platform]
b = 10;                     %mN*m*s/rad         [Viscous Friction of U-Joint]
g = 9810;                   %mm/s^2             [Gravitational Acceleration]
I_b = (2/5)*m_b*r_b^2;      %g mm^2             [Moment of Inertia of Solid Ball]
%% Simplification Variables
%First we define a set of simplifcation variables in terms of the variables
%defined above. This allows us to write simplified state space equations
%that are not overly complicated.

A = - (m_b*r_b^2 + m_b*r_c*r_b+I_b)/r_b;
C = -(m_b*r_b^2+I_b)/r_b;
D = -(m_b*r_b^2+m_b*r_c*r_b^2+I_b*r_b)/r_b;
H = (I_b*r_b+I_p*r_b+m_b*r_b^2+m_b*r_b*r_c^2+2*m_b*r_b^2*r_c+m_p*r_b*r_g^2)/r_b;
I = A*D+C*H;
J = -g*m_b*(r_b+r_c)-g*m_p*r_g-g*m_b*r_b;
K = -g*m_b*r_b;

%% Defining the State Space Model
%Now we define the state space matrices and model using the simplified
%variables defined above.

A_mat = [0,      0,          1,     0;
         0,      0,          0,     1;
         0, (1/I)*(D*J+H*K), 0,  (D*b/I);
         0, (1/I)*(A*K-C*J), 0, -(C*b)/I];
     
B_mat = [0;
         0;
         (D/I)*(l_p/r_m);
        -(C/I)*(l_p/r_m)];

% C_mat = eye(4);

% D_mat = zeros(4,1);

dX = A_mat*X + B_mat*T;
