# -*- coding: utf-8 -*-
"""
Created on Tue Feb  2 17:35:51 2021

@author: hallm
"""


state = 0



#<<<<<>>>>>-----------------Run Finite State Machine-----------------<<<<<>>>>>
while True:
    
    #<<<<<-----State 0 - Initialization----------------------------------->>>>>   
    if state == 0:
        import pyb
        import array
        from pyb import UART
        
        pyb.enable_irq
        tim = pyb.Timer(1,freq(100))
        ser = UART(2)
        adc = pyb.ADC(pyb.pin.board.PC13)
        
        adcBuf = array.array('H', (0 for index in range(1000)))

    #<<<<<-----State 1 - Wait for Keypress-------------------------------->>>>>
    if state == 1:
        pass

    #<<<<<-----State 2 - Collect Button Pin Data-------------------------->>>>>
    if state == 2:
        
        adc.read_timed(buf, 100)
        
    #<<<<<-----State 3 - Send Serial Data--------------------------------->>>>>        
    if state == 3:
        pass

