# -*- coding: utf-8 -*-
"""
@file ME405-Lab0x03-Board-RHall.py
@brief Nucleo-side script to measure the button step response and transmit to PC.
@detail This script utilizes the Nucleo's Analog to Digital Converter (ADC) to
measure the step response of the pin connected to the user button. This script
loops continuously to allow for multiple takes without having to reset the Nucleo.
To begin the script, the Nucleo waits for a command ('g') to be sent across the
serial port. Once recieved, the Nucleo waits for the button to be pressed. Once the
button is pressed, a 'prebuffer' for the ADC loops continuously at the timer frequency
(200 kHz), reading the essentially 'zero' data that occurs before the button response.
Once the voltage passes a certain threshold (33/4294 Volts), it begins collection of
the step response data. Once data is collected, it sends it in batch over the serial
port and idles until it recieves over the serial port again.

@author Rick Hall

@date Tue Feb  2 17:35:51 2021
"""

    
#<<<<<-----Section 0 - Initialization----------------------------------->>>>>   
import pyb
import array
from pyb import UART
import micropython
    
# Enable Interupt Service Routines
pyb.enable_irq

# Create emergency buffer to store errors in ISR
micropython.alloc_emergency_exception_buf(200)

# Initialize necessary hardware components
tim = pyb.Timer(1,freq=200000)
ser = UART(2)
btn = pyb.Pin(pyb.Pin.board.PA0, mode=pyb.Pin.IN)
adc = pyb.ADC(btn)
                
# Initialize ADC Buffer
adcBuf = array.array('H', (0 for index in range(500)))
preBuf = array.array('H', (0 for index in range(20)))
        
# Initialize Variables
keyBool = 0     # Valid Keypress Boolean - only goes high when valid key has been recieved
runADC = 0      # ADC Run Boolean - raised to tell ADC to run.
            
# Set up Button Press ISR
def btnPress(line):
    '''
    @brief Callback function for the button to start ADC. Sets two booleans.

    '''
    global keyBool
    global runADC
    if keyBool == 1:
        runADC = 1
        keyBool = 0
        
        
extint = pyb.ExtInt(pyb.Pin.board.PC13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_NONE, callback=btnPress)
        
#<<<<<>>>>>-----------------Run Looping Script-----------------------<<<<<>>>>>
while True:
    
    #<<<<<-----Section 1 - Wait for Keypress-------------------------------->>>>>
    while True:
        # Wait for keypress
        if ser.any() != 0:
            # Check to see if proper character recieved & send apppropriate msg
            val = ser.readchar()
            ser.write('Recieved ' + str(chr(val)) + '\n')
            if str(chr(val)) == 'g':
                keyBool = 1
                break
            else:
                ser.write('Invalid Input \n')

    #<<<<<-----State 2 - Collect Button Pin Data-------------------------->>>>>
    while True:
        # Wait for button press; start ADC when button is pressed
        if runADC == 1:
            # Run looping prebuffer to eliminate excessive zero data
            adc.read_timed(preBuf, tim)
            if preBuf[-1] >= 10:
                break
    # Run main buffer when ADC when notable voltage change is detected
    adc.read_timed(adcBuf, tim)
    runADC = 0
     
        
    #<<<<<-----State 3 - Send Serial Data--------------------------------->>>>>
    
    # Send data to Serial Port
    ser.write(str(adcBuf) + '\n')
    while ser.any() != 0:
        pass

            
            
