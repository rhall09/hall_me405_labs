# -*- coding: utf-8 -*-
"""
@file ME405-Lab0x03-PC-RHall.py
@brief PC-side script to prompt Nucleo to measure the button step response, recieve data over the serial port, and process data.
@details This script prompts the Nucleo to run a step response test by measuring
the Nucleo pin corresponding to the user button with the Analog to Digital Converter (ADC).
To direct the nucleo script to begin, a 'g' key must be sent over the serial port. Due to
significant flaws in the Python keyboard module, as well as the lack of multitasking in
this script, the built-in Input function was utilized to recieve user input. Once 'g'
has been sent to the Nucleo, the user will be prompted to press the user button. The
Nucleo will read the step response and send the ADC Data over the serial port as a batch.
This script decodes the recieved data (which unfortunately has to be converted to a string
and sent as ASCII characters rather than sent byte by byte) and displays it in a plot and
saves it in a CSV file.

@author Rick Hall

@date Tue Feb  2 17:35:51 2021
"""

  
#<<<<<-----Section 0 - Initialization--------------------------------->>>>>   
import numpy as np
from matplotlib import pyplot as plt
import csv
import serial

# Set up Serial Port
ser = serial.Serial(port='COM5',baudrate=115273,timeout=1)

val = 0

# Function to send keypress to Serial Port                     
def sendChar(inv):
    '''
    @brief Short function to send a keypress over the serial port and recieve a reply.
    
    @param inv Input to be sent to the Serial Port
    
    @return myval String response from the Serial Port
    '''
    # Write string input to serial port
    ser.write(str(inv).encode('ascii'))
    
    # Read serial port response and return
    myval = ser.readline().decode('ascii')
    return myval
    
#<<<<<>>>>>-----------------Run Looping Script-----------------------<<<<<>>>>>
while True:
      
    #<<<<<-----Section 1 - Wait for Keypress------------------------------>>>>>
    while True:
        # Wait for Keypress
        val = input('\n'*50 + 'Press g, then press enter! ----->>>   ')
        # If correct Keypress, send to Serial Port
        if val == 'g':
            print('\n'*50)
            print('Nucleo' + sendChar(val))
            print('Now press the button!')
            break
        # Otherwise, ignore & print error message
        else:
            print('\n'*50 + 'Wrong Key! Press g!')
    
    #<<<<<-----Section 2 - Recieve Serial Data------------------------------>>>>>
    while True:
        # Wait until data is sent by Serial Port & Decode
        if ser.in_waiting != 0:
            dat = ser.readline().decode('ascii')
            dat_list = dat.strip("array('H', [  ]\n)").split(', ')
            for n in range(len(dat_list)):
                dat_list[n] = int(dat_list[n])
            print('\n'*50 + 'Data Recieved!' + '\n'*5)
            break
                  
    #<<<<<-----State 3 - Plot Array & Store CSV--------------------------->>>>>
    # Run computations to convert to Volts and set up arbitrary Time index        
    volt_factor = 3.3/4095
    time_step = 1/200000
    time = []
    volts = np.multiply(dat_list,volt_factor)
    for inx in range(len(dat_list)):
        time.append(inx * time_step)
        
    # Determine Time Constant
    point = np.max(volts) * 0.613
    min_inx = np.argmin(np.abs([x - point for x in volts]))
    tau = time[min_inx]
    
    tau_us = round(tau*1E6)
    
    print("Step response has a time constant of " + str(tau_us) + " microseconds!" + '\n'*5)
    
    # Plot voltage step response
    plt.plot(time, volts,'k')
    plt.plot(tau,point,'ro')
    plt.text(1.1*tau, 0.9*point, ("Time Constant = " + str(tau_us) + " us"))
    plt.xlabel('Time [s]')
    plt.ylabel('Voltage [V]')
    plt.savefig('Lab0x03_Button_Step.jpg')
    
    # Save data to CSV file
    filename = 'Lab0x03_ADC_Output.csv'
    with open(filename, 'w', newline='') as csv_file:
        write = csv.writer(csv_file)
        write.writerow(['Time [s]', 'Voltage [V]'])
        for n in range(len(time)):
            write.writerow([time[n],volts[n]])
    
    # End script - Comment out to loop
    break

# Close serial port to allow other programs to access the board
ser.close()
        
