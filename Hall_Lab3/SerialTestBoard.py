# -*- coding: utf-8 -*-
"""
@file ME405-Lab0x03-Board-RHall.py

Created on Tue Feb  2 17:01:39 2021

@author: hallm
"""

from pyb import UART

ser = UART(2)

while True:
    if ser.any() != 0:
        val = ser.readchar()
        val = chr(val)
        ser.write('Recieved ' + str(val))