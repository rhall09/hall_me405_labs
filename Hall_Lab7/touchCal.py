# -*- coding: utf-8 -*-
"""
@file touchCal.py

@brief Simple script to collect touch panel data for calibration
@details This script reads data from the touch panel using the touchChk script. 
it records outputs from the panel and stores them to a CSV. The output from the 
panel are in digital volts - a twelve bit representation of a range from 0V to 
3.3V. As the digital reading has no clear units, it has been described as 
'bits' for lack of a more clear unit. To run testing, the panel was tested for
inputs across the x direction and across the y, then placed in an excel scatter
plot relative to read time. This allowed for the simultaneous analysis of 
average read time for the touchChk script and the approximation of minimum and 
maximum read values in the x and y directions. These min/max values were used 
to calibrate the readings rom the touchChk script, utilized to convert the 
output to mm in the touchPanel script.

@package Simple script to collect touch panel data for calibration

This script reads data from the touch panel using the touchChk script. 
it records outputs from the panel and stores them to a CSV. The output from the 
panel are in digital volts - a twelve bit representation of a range from 0V to 
3.3V. As the digital reading has no clear units, it has been described as 
'bits' for lack of a more clear unit. To run testing, the panel was tested for
inputs across the x direction and across the y, then placed in an excel scatter
plot relative to read time. This allowed for the simultaneous analysis of 
average read time for the touchChk script and the approximation of minimum and 
maximum read values in the x and y directions. These min/max values were used 
to calibrate the readings rom the touchChk script, utilized to convert the 
output to mm in the touchPanel script.

@author Rick Hall

@date Wed Mar  3 09:09:48 2021
"""

import pyb
import utime
from touchChk import touchChk
import array as ar

tch = touchChk('PA0','PA6','PA1','PA7',1,1,[0, 0])

tBuf = ar.array('h',[])

with open ("TouchCal.csv", "w") as csvFile:
    csvFile.write('Time [s], Xpos [bits], Ypos [bits], Zpos[bits]\n')
    while True:
        try:
            input('Press Enter to Begin Test Reading \n     >>>   ')
            while True:
                t0 = utime.ticks_us()
                tch.read()
                t1 = utime.ticks_us()
                tdif = utime.ticks_diff(t1,t0)
    
                line = str(tdif) + ', ' + str(tch.pos[0]) + ', ' + str(tch.pos[1]) + ', ' + str(tch.pos[2]) + '\n'
                csvFile.write(line)
            
        except KeyboardInterrupt:
            print('\n'*50 + 'Sucessfuly Concluded Test\n\n')
            break 