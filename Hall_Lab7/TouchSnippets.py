# -*- coding: utf-8 -*-
"""
Created on Tue Mar  2 23:51:25 2021

@author: hallm
"""

    def Fastread(self):
        '''
        @brief Reads all 3 touch sensor channels in turn
        @detail 

        '''
        if self.tch == False:
            #----------------zread()-------------------------------------------
            # Reset Pins for z-read in Optimal Configuration
            if self.dset == 2:
            # If the previous set was Y, Configure s.t. ADC & High already set:
                
                # self.phigh = pyb.Pin(self.yp, mode=pyb.Pin.OUT_PP)
                # self.phigh.high()
                self.plow  = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
                self.plow.low()
                
                self.pfloat = pyb.Pin(self.ym, mode=pyb.Pin.IN)
                # self.pread = pyb.ADC(self.xm)
                self.dset = 4
                
            elif self.dset == 1:
            # If the previous set was X, Configure s.t. ADC & Low already set:
                                
                self.phigh = pyb.Pin(self.yp, mode=pyb.Pin.OUT_PP)
                self.phigh.high()
                # self.plow  = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
                # self.plow.low()
                
                self.pfloat = pyb.Pin(self.xm, mode=pyb.Pin.IN)
                # self.pread = pyb.ADC(self.ym)
                self.dset = 3
    
            
            # Read Sensor Data & filter
            
            utime.sleep_us(5)
            self.zbuf[0] = self.pread.read()
            utime.sleep_us(5)
            self.zbuf[1] = self.pread.read()
            utime.sleep_us(5)
            self.zbuf[2] = self.pread.read()
            utime.sleep_us(5)
            self.zbuf[3] = self.pread.read()
            utime.sleep_us(5)
            self.zbuf[4] = self.pread.read()
            
            self.pos[2] = round((self.zbuf[0]+self.zbuf[1]+self.zbuf[2]+self.zbuf[3]+self.zbuf[4])/5)
            
            # If ADC reads less than high, or greater than low, touch detected:
            if 16 < self.pos[2] < 4080:
                self.tch = True
            else:
                self.tch = False
            #----------------zread()-------------------------------------------
            
            if self.tch == True:
                #------------xread()-------------------------------------------
                # Reset Pins for x-read in Optimal Configuration
                if self.dset == 2 or self.dset == 4: # Y or Zy
                # If the previous set was Y or Zy, Configure all:
                    
                    self.phigh = pyb.Pin(self.xm, mode=pyb.Pin.OUT_PP)
                    self.phigh.high()
                    self.plow  = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
                    self.plow.low()
                    
                    self.pfloat = pyb.Pin(self.yp, mode=pyb.Pin.IN)
                    self.pread = pyb.ADC(self.ym)
                    self.dset = 1
                    
                elif self.dset == 3: # Zx
                # If the previous set was Zx, Configure s.t. ADC & Low already set:
        
                    self.phigh = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
                    self.phigh.high()
                    # self.plow  = pyb.Pin(self.xm, mode=pyb.Pin.OUT_PP)
                    # self.plow.low()
                    
                    self.pfloat = pyb.Pin(self.yp, mode=pyb.Pin.IN)
                    # self.pread = pyb.ADC(self.ym)
                    self.dset = 1            
                
                # Read Sensor Data & filter
                
                utime.sleep_us(5)
                self.xbuf[0] = self.pread.read()
                utime.sleep_us(5)
                self.xbuf[1] = self.pread.read()
                utime.sleep_us(5)
                self.xbuf[2] = self.pread.read()
                utime.sleep_us(5)
                self.xbuf[3] = self.pread.read()
                utime.sleep_us(5)
                self.xbuf[4] = self.pread.read()
                
                self.pos[0] = round((self.xbuf[0]+self.xbuf[1]+self.xbuf[2]+self.xbuf[3]+self.xbuf[4])/5)
                #------------xread()-------------------------------------------
                #------------yread()-------------------------------------------
                # Reset Pins for y-read in Optimal Configuration
                if self.dset == 1 or self.dset == 3: # X or Zx
                # If the previous set was X or Zx, Configure all:
                        
                    self.phigh = pyb.Pin(self.yp, mode=pyb.Pin.OUT_PP)
                    self.phigh.high()
                    self.plow  = pyb.Pin(self.ym, mode=pyb.Pin.OUT_PP)
                    self.plow.low()
                    
                    self.pfloat = pyb.Pin(self.xp, mode=pyb.Pin.IN)
                    self.pread = pyb.ADC(self.xm)
                    self.dset = 2
                    
                elif self.dset == 4: # Zy
                # If the previous set was Zy, Configure s.t. ADC & High already set:
                                    
                    # self.phigh = pyb.Pin(self.yp, mode=pyb.Pin.OUT_PP)
                    # self.phigh.high()
                    self.plow  = pyb.Pin(self.ym, mode=pyb.Pin.OUT_PP)
                    self.plow.low()
                    
                    self.pfloat = pyb.Pin(self.xp, mode=pyb.Pin.IN)
                    # self.pread = pyb.ADC(self.xm)
                    self.dset = 2
        
                
                # Read Sensor Data & filter
                
                utime.sleep_us(5)
                self.ybuf[0] = self.pread.read()
                utime.sleep_us(5)
                self.ybuf[1] = self.pread.read()
                utime.sleep_us(5)
                self.ybuf[2] = self.pread.read()
                utime.sleep_us(5)
                self.ybuf[3] = self.pread.read()
                utime.sleep_us(5)
                self.ybuf[4] = self.pread.read()
                
                self.pos[1] = round((self.ybuf[0]+self.ybuf[1]+self.ybuf[2]+self.ybuf[3]+self.ybuf[4])/5)
                #------------yread()-------------------------------------------
        else:
            if self.dset == 1:
                #------------xread()-------------------------------------------
                # Reset Pins for x-read in Optimal Configuration
                if self.dset == 2 or self.dset == 4: # Y or Zy
                # If the previous set was Y or Zy, Configure all:
                    
                    self.phigh = pyb.Pin(self.xm, mode=pyb.Pin.OUT_PP)
                    self.phigh.high()
                    self.plow  = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
                    self.plow.low()
                    
                    self.pfloat = pyb.Pin(self.yp, mode=pyb.Pin.IN)
                    self.pread = pyb.ADC(self.ym)
                    self.dset = 1
                    
                elif self.dset == 3: # Zx
                # If the previous set was Zx, Configure s.t. ADC & Low already set:
        
                    self.phigh = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
                    self.phigh.high()
                    # self.plow  = pyb.Pin(self.xm, mode=pyb.Pin.OUT_PP)
                    # self.plow.low()
                    
                    self.pfloat = pyb.Pin(self.yp, mode=pyb.Pin.IN)
                    # self.pread = pyb.ADC(self.ym)
                    self.dset = 1            
                
                # Read Sensor Data & filter
                
                utime.sleep_us(5)
                self.xbuf[0] = self.pread.read()
                utime.sleep_us(5)
                self.xbuf[1] = self.pread.read()
                utime.sleep_us(5)
                self.xbuf[2] = self.pread.read()
                utime.sleep_us(5)
                self.xbuf[3] = self.pread.read()
                utime.sleep_us(5)
                self.xbuf[4] = self.pread.read()
                
                self.pos[0] = round((self.xbuf[0]+self.xbuf[1]+self.xbuf[2]+self.xbuf[3]+self.xbuf[4])/5)
                #------------xread()-------------------------------------------
                #----------------zread()---------------------------------------
                # Reset Pins for z-read in Optimal Configuration
                if self.dset == 2:
                # If the previous set was Y, Configure s.t. ADC & High already set:
                    
                    # self.phigh = pyb.Pin(self.yp, mode=pyb.Pin.OUT_PP)
                    # self.phigh.high()
                    self.plow  = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
                    self.plow.low()
                    
                    self.pfloat = pyb.Pin(self.ym, mode=pyb.Pin.IN)
                    # self.pread = pyb.ADC(self.xm)
                    self.dset = 4
                    
                elif self.dset == 1:
                # If the previous set was X, Configure s.t. ADC & Low already set:
                                    
                    self.phigh = pyb.Pin(self.yp, mode=pyb.Pin.OUT_PP)
                    self.phigh.high()
                    # self.plow  = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
                    # self.plow.low()
                    
                    self.pfloat = pyb.Pin(self.xm, mode=pyb.Pin.IN)
                    # self.pread = pyb.ADC(self.ym)
                    self.dset = 3
        
                
                # Read Sensor Data & filter
                
                utime.sleep_us(5)
                self.zbuf[0] = self.pread.read()
                utime.sleep_us(5)
                self.zbuf[1] = self.pread.read()
                utime.sleep_us(5)
                self.zbuf[2] = self.pread.read()
                utime.sleep_us(5)
                self.zbuf[3] = self.pread.read()
                utime.sleep_us(5)
                self.zbuf[4] = self.pread.read()
                
                self.pos[2] = round((self.zbuf[0]+self.zbuf[1]+self.zbuf[2]+self.zbuf[3]+self.zbuf[4])/5)
                
                # If ADC reads less than high, or greater than low, touch detected:
                if 16 < self.pos[2] < 4080:
                    self.tch = True
                else:
                    self.tch = False
                #----------------zread()---------------------------------------
                #------------yread()-------------------------------------------
                # Reset Pins for y-read in Optimal Configuration
                if self.dset == 1 or self.dset == 3: # X or Zx
                # If the previous set was X or Zx, Configure all:
                        
                    self.phigh = pyb.Pin(self.yp, mode=pyb.Pin.OUT_PP)
                    self.phigh.high()
                    self.plow  = pyb.Pin(self.ym, mode=pyb.Pin.OUT_PP)
                    self.plow.low()
                    
                    self.pfloat = pyb.Pin(self.xp, mode=pyb.Pin.IN)
                    self.pread = pyb.ADC(self.xm)
                    self.dset = 2
                    
                elif self.dset == 4: # Zy
                # If the previous set was Zy, Configure s.t. ADC & High already set:
                                    
                    # self.phigh = pyb.Pin(self.yp, mode=pyb.Pin.OUT_PP)
                    # self.phigh.high()
                    self.plow  = pyb.Pin(self.ym, mode=pyb.Pin.OUT_PP)
                    self.plow.low()
                    
                    self.pfloat = pyb.Pin(self.xp, mode=pyb.Pin.IN)
                    # self.pread = pyb.ADC(self.xm)
                    self.dset = 2
        
                
                # Read Sensor Data & filter
                
                utime.sleep_us(5)
                self.ybuf[0] = self.pread.read()
                utime.sleep_us(5)
                self.ybuf[1] = self.pread.read()
                utime.sleep_us(5)
                self.ybuf[2] = self.pread.read()
                utime.sleep_us(5)
                self.ybuf[3] = self.pread.read()
                utime.sleep_us(5)
                self.ybuf[4] = self.pread.read()
                
                self.pos[1] = round((self.ybuf[0]+self.ybuf[1]+self.ybuf[2]+self.ybuf[3]+self.ybuf[4])/5)
                #------------yread()-------------------------------------------
            else:
                #------------yread()-------------------------------------------
                # Reset Pins for y-read in Optimal Configuration
                if self.dset == 1 or self.dset == 3: # X or Zx
                # If the previous set was X or Zx, Configure all:
                        
                    self.phigh = pyb.Pin(self.yp, mode=pyb.Pin.OUT_PP)
                    self.phigh.high()
                    self.plow  = pyb.Pin(self.ym, mode=pyb.Pin.OUT_PP)
                    self.plow.low()
                    
                    self.pfloat = pyb.Pin(self.xp, mode=pyb.Pin.IN)
                    self.pread = pyb.ADC(self.xm)
                    self.dset = 2
                    
                elif self.dset == 4: # Zy
                # If the previous set was Zy, Configure s.t. ADC & High already set:
                                    
                    # self.phigh = pyb.Pin(self.yp, mode=pyb.Pin.OUT_PP)
                    # self.phigh.high()
                    self.plow  = pyb.Pin(self.ym, mode=pyb.Pin.OUT_PP)
                    self.plow.low()
                    
                    self.pfloat = pyb.Pin(self.xp, mode=pyb.Pin.IN)
                    # self.pread = pyb.ADC(self.xm)
                    self.dset = 2
        
                
                # Read Sensor Data & filter
                
                utime.sleep_us(5)
                self.ybuf[0] = self.pread.read()
                utime.sleep_us(5)
                self.ybuf[1] = self.pread.read()
                utime.sleep_us(5)
                self.ybuf[2] = self.pread.read()
                utime.sleep_us(5)
                self.ybuf[3] = self.pread.read()
                utime.sleep_us(5)
                self.ybuf[4] = self.pread.read()
                
                self.pos[1] = round((self.ybuf[0]+self.ybuf[1]+self.ybuf[2]+self.ybuf[3]+self.ybuf[4])/5)
                #------------yread()-------------------------------------------
                #----------------zread()---------------------------------------
                # Reset Pins for z-read in Optimal Configuration
                if self.dset == 2:
                # If the previous set was Y, Configure s.t. ADC & High already set:
                    
                    # self.phigh = pyb.Pin(self.yp, mode=pyb.Pin.OUT_PP)
                    # self.phigh.high()
                    self.plow  = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
                    self.plow.low()
                    
                    self.pfloat = pyb.Pin(self.ym, mode=pyb.Pin.IN)
                    # self.pread = pyb.ADC(self.xm)
                    self.dset = 4
                    
                elif self.dset == 1:
                # If the previous set was X, Configure s.t. ADC & Low already set:
                                    
                    self.phigh = pyb.Pin(self.yp, mode=pyb.Pin.OUT_PP)
                    self.phigh.high()
                    # self.plow  = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
                    # self.plow.low()
                    
                    self.pfloat = pyb.Pin(self.xm, mode=pyb.Pin.IN)
                    # self.pread = pyb.ADC(self.ym)
                    self.dset = 3
        
                
                # Read Sensor Data & filter
                
                utime.sleep_us(5)
                self.zbuf[0] = self.pread.read()
                utime.sleep_us(5)
                self.zbuf[1] = self.pread.read()
                utime.sleep_us(5)
                self.zbuf[2] = self.pread.read()
                utime.sleep_us(5)
                self.zbuf[3] = self.pread.read()
                utime.sleep_us(5)
                self.zbuf[4] = self.pread.read()
                
                self.pos[2] = round((self.zbuf[0]+self.zbuf[1]+self.zbuf[2]+self.zbuf[3]+self.zbuf[4])/5)
                
                # If ADC reads less than high, or greater than low, touch detected:
                if 16 < self.pos[2] < 4080:
                    self.tch = True
                else:
                    self.tch = False
                #----------------zread()---------------------------------------
                #------------xread()-------------------------------------------
                # Reset Pins for x-read in Optimal Configuration
                if self.dset == 2 or self.dset == 4: # Y or Zy
                # If the previous set was Y or Zy, Configure all:
                    
                    self.phigh = pyb.Pin(self.xm, mode=pyb.Pin.OUT_PP)
                    self.phigh.high()
                    self.plow  = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
                    self.plow.low()
                    
                    self.pfloat = pyb.Pin(self.yp, mode=pyb.Pin.IN)
                    self.pread = pyb.ADC(self.ym)
                    self.dset = 1
                    
                elif self.dset == 3: # Zx
                # If the previous set was Zx, Configure s.t. ADC & Low already set:
        
                    self.phigh = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
                    self.phigh.high()
                    # self.plow  = pyb.Pin(self.xm, mode=pyb.Pin.OUT_PP)
                    # self.plow.low()
                    
                    self.pfloat = pyb.Pin(self.yp, mode=pyb.Pin.IN)
                    # self.pread = pyb.ADC(self.ym)
                    self.dset = 1            
                
                # Read Sensor Data & filter
                
                utime.sleep_us(5)
                self.xbuf[0] = self.pread.read()
                utime.sleep_us(5)
                self.xbuf[1] = self.pread.read()
                utime.sleep_us(5)
                self.xbuf[2] = self.pread.read()
                utime.sleep_us(5)
                self.xbuf[3] = self.pread.read()
                utime.sleep_us(5)
                self.xbuf[4] = self.pread.read()
                
                self.pos[0] = round((self.xbuf[0]+self.xbuf[1]+self.xbuf[2]+self.xbuf[3]+self.xbuf[4])/5)
                #------------xread()-------------------------------------------                
