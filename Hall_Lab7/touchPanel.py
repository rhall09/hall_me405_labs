# -*- coding: utf-8 -*-
"""
@file TouchPanel.py

@brief Class script to read a capacitive touch panel

@details This script contains a class 'touchPanel' that reads a resistive touch 
panel. This class was developed for the Balancing Table Lab for ME 405. 
This script is calibrated to an origin at the center of the panel such 
that it will return a location, in mm, with the positive directions 'x' and 
'y' being oriented toward the driving motors for the table. The 'z' 
direction a boolean that simply indicates contact with the screen. 
Contained within this class are four methods - one for each read direction 
and one to read all three directions. These methods are optimized such that 
the minimum number of pin calibrations are required. Additionally, three
samples are taken of each measurement and averaged to minimize the 
noise in the reading. 

@package Class script to read a capacitive touch panel

This script contains a class 'touchPanel' that reads a resistive touch 
panel. This class was developed for the Balancing Table Lab for ME 405. 
This script is calibrated to an origin at the center of the panel such 
that it will return a location, in mm, with the positive directions 'x' and 
'y' being oriented toward the driving motors for the table. The 'z' 
direction a boolean that simply indicates contact with the screen. 
Contained within this class are four methods - one for each read direction 
and one to read all three directions. These methods are optimized such that 
the minimum number of pin calibrations are required. Additionally, three
samples are taken of each measurement and averaged to minimize the 
noise in the reading. 

@author Rick Hall

@date Thu Feb 25 08:30:55 2021

execfile('touchPanel.py')
"""

import pyb
import utime
import array
# import touchDisp

class TouchPanel:
    '''
    @brief This class will establish and read from a touch panel object. 
    
    @details This class operates a resisitive touch panel, developed for the 
    Balancing Table Lab for ME 405. This script is calibrated to an origin at 
    the center of the panel such 
    that it will return a location, in mm, with the positive directions 'x' and 
    'y' being oriented toward the driving motors for the table. The 'z' 
    direction a boolean that simply indicates contact with the screen. 
    Contained within this class are four methods - one for each read direction 
    and one to read all three directions. These methods are optimized such that 
    the minimum number of pin calibrations are required. Additionally, three
    samples are taken of each measurement and averaged to minimize the 
    noise in the reading. 
    '''    
    def __init__(self,xp,xm,yp,ym,P_width,P_length,P_center=[2048,2048]):
        '''
        @brief Initialization function for the touchPanel class
        
        This function initializes the touchPanel class. As the panel 
        is somewhat specific to this application (namely that of the ME 405 
        balancing table lab), all panel and pin parameters are internally 
        defined in the class. The panel is initialized to read the 'z' 
        direction to determine contact, but will function with any read 
        request. Additionally, the class contains an array with the current 
        touch position as well as a boolean to indicate contact. 
        
        @param xp Pin for the positive-x direction, input as a string (ex: 'PA0')
        @param xm Pin for the negative-x direction, input as a string (ex: 'PA6')
        @param yp Pin for the positive-y direction, input as a string (ex: 'PA1')
        @param ym Pin for the negative-y direction, input as a string (ex: 'PA7')
        @param P_width Width of the touch panel (Y-direction) in mm (integer or float)
        @param P_length Length of the tuch panel (X-direction) in mm (integer or float)
        @param P_center Calibrated center of the panel, list of 2 values [x,y]
        '''
        
        ## Array containing the last reading from all 3 read directions
        self.pos = array.array('f',[0, 0, 0])
        
        ## Boolean indicating if there is contact with the panel
        self.tch = False
        
        ## Calibration array - panel specific; be sure to test your own panel for voltage limits. Experimentally determined - do not alter without reason.
        self.Cal = array.array('h',[300, 3820, 480, 3650])
        
        ## Y-direction calibration constant. Internally computed.
        self.yCal = P_width/(self.Cal[3]-self.Cal[2])
        
        ## X-direction calibration constant. Internally computed.
        self.xCal = P_length/(self.Cal[1]-self.Cal[0])
        
        ## Center point calibraiton. Defaults to [2048,2048], more accurate center must be experimentally determined.
        self.cCal = P_center
        
        ## Buffer to store last 3 X-positions
        self.xbuf = array.array('h',[0, 0, 0])
        
        ## Buffer to store last 3 Y-positions
        self.ybuf = array.array('h',[0, 0, 0])
        
        ## Buffer to store last 3 Z-positions
        self.zbuf = array.array('h',[0, 0, 0])
        
        ## Pin object for negative-X pin
        self.xm = pyb.Pin(xm) 
        
        ## Pin object for positive-Y pin
        self.yp = pyb.Pin(yp)
        
        ## Pin object for positive-X pin
        self.xp = pyb.Pin(xp)
        
        ## Pin object for negative-Y pin
        self.ym = pyb.Pin(ym) 
 
    
        ## Output pin for high pin in panel read arrangement. Altered for each direction.
        self.phigh = pyb.Pin(self.ym, mode=pyb.Pin.OUT_PP)
        self.phigh.high()
        
        ## Output pin for low pin in panel read arrangement. Altered for each direction.
        self.plow  = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
        self.plow.low()
        
        ## Input pin for read arrangement - allows pin voltage to float. Altered for each direction.
        self.pfloat = pyb.Pin(self.yp, mode=pyb.Pin.IN)
        
        ## ADC read pin for read arrangement. Altered for each direction.
        self.pread = pyb.ADC(self.xm)
        
        ## Direction set indicator. Facilitates optimization of pin transitions - do not alter without reason.
        self.dset = 4
     
    # Method to read X direction only -------------------------------------||||
    def xread(self):
        '''
        @brief Read X-direction of Touch Panel
        
        This method reads the touch panel in the 'x' direction. It will 
        function regardless of the previous read, but operates best in 
        transition from 'Zx.' The reading is filtered by taking three 
        measurements and averaging them. The readings are 5us apart to minimize 
        error due to settling time. This method sets the 'x' value in the 
        class internal position array to a value in mm.
        '''
        # Reset Pins for x-read in Optimal Configuration
        if self.dset == 2 or self.dset == 4: # Y or Zy
        # If the previous set was Y or Zy, Configure all:
            
            self.phigh = pyb.Pin(self.xm, mode=pyb.Pin.OUT_PP)
            self.phigh.high()
            self.plow  = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
            self.plow.low()
            
            self.pfloat = pyb.Pin(self.ym, mode=pyb.Pin.IN)
            self.pread = pyb.ADC(self.yp)
            self.dset = 1
            
        elif self.dset == 3: # Zx
        # If the previous set was Zx, Configure s.t. ADC & Low already set:

            self.phigh = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
            self.phigh.high()
            # self.plow  = pyb.Pin(self.xm, mode=pyb.Pin.OUT_PP)
            # self.plow.low()
            
            self.pfloat = pyb.Pin(self.ym, mode=pyb.Pin.IN)
            # self.pread = pyb.ADC(self.yp)
            self.dset = 1            
        
        # Read Sensor Data & filter
        utime.sleep_us(5)
        self.xbuf[0] = self.pread.read()
        utime.sleep_us(5)
        self.xbuf[1] = self.pread.read()
        utime.sleep_us(5)
        self.xbuf[2] = self.pread.read()
        self.pos[0] = round((self.xbuf[0]+self.xbuf[1]+self.xbuf[2])/3)

        self.pos[0] = round(self.xCal*(self.pos[0]-self.cCal[0]),2)
    
    # Method to read Y direction only -------------------------------------||||
    def yread(self):
        '''
        @brief Read Y-direction of Touch Panel
        
        This method reads the touch panel in the 'y' direction. It will 
        function regardless of the previous read, but operates best in 
        transition from 'Zy.' The reading is filtered by taking three 
        measurements and averaging them. The readings are 5us apart to minimize 
        error due to settling time. This method sets the 'y' value in the 
        class internal position array to a value in mm.
        '''
        # Reset Pins for y-read in Optimal Configuration
        if self.dset == 1 or self.dset == 3: # X or Zx
        # If the previous set was X or Zx, Configure all:
                
            self.phigh = pyb.Pin(self.ym, mode=pyb.Pin.OUT_PP)
            self.phigh.high()
            self.plow  = pyb.Pin(self.yp, mode=pyb.Pin.OUT_PP)
            self.plow.low()
            
            self.pfloat = pyb.Pin(self.xp, mode=pyb.Pin.IN)
            self.pread = pyb.ADC(self.xm)
            self.dset = 2
            
        elif self.dset == 4: # Zy
        # If the previous set was Zy, Configure s.t. ADC & High already set:
                            
            # self.phigh = pyb.Pin(self.ym, mode=pyb.Pin.OUT_PP)
            # self.phigh.high()
            self.plow  = pyb.Pin(self.yp, mode=pyb.Pin.OUT_PP)
            self.plow.low()
            
            self.pfloat = pyb.Pin(self.xp, mode=pyb.Pin.IN)
            # self.pread = pyb.ADC(self.xm)
            self.dset = 2

        
        # Read Sensor Data & filter
        utime.sleep_us(5)
        self.ybuf[0] = self.pread.read()
        utime.sleep_us(5)
        self.ybuf[1] = self.pread.read()
        utime.sleep_us(5)
        self.ybuf[2] = self.pread.read()
        self.pos[1] = round((self.ybuf[0]+self.ybuf[1]+self.ybuf[2])/3)
        
        self.pos[1] = round(self.yCal*(self.pos[1]-self.cCal[1]),2)
    
    # Method to Test Contact-----------------------------------------------||||
    def zread(self):
        '''
        @brief Read Z-direction of Touch Panel
        
        This method reads the touch panel in the 'z' direction. When 
        called after an 'x' or 'y' reading, this method selects one of two 
        configurations ('Zx' or 'Zy') to minimize the required number of pin 
        transitions. The reading is filtered by taking three measurements 5 us
        apart and averaging them. This method sets the 'z' value in the 
        class internal position array to a value in bits (for debuging) and 
        sets the touch boolean if the read pin is not high or low (reading in
        between).
        '''
        
        # Reset Pins for z-read in Optimal Configuration
        if self.dset == 2:
        # If the previous set was Y, Configure s.t. ADC & High already set:
                
            # self.phigh = pyb.Pin(self.ym, mode=pyb.Pin.OUT_PP)
            # self.phigh.high()
            self.plow  = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
            self.plow.low()
            
            self.pfloat = pyb.Pin(self.yp, mode=pyb.Pin.IN)
            # self.pread = pyb.ADC(self.xm)
            self.dset = 4
            
        elif self.dset == 1:
        # If the previous set was X, Configure s.t. ADC & Low already set:
                            
            self.phigh = pyb.Pin(self.ym, mode=pyb.Pin.OUT_PP)
            self.phigh.high()
            # self.plow  = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
            # self.plow.low()
            
            self.pfloat = pyb.Pin(self.xm, mode=pyb.Pin.IN)
            # self.pread = pyb.ADC(self.yp)
            self.dset = 3

        
        # Read Sensor Data & filter
        
        utime.sleep_us(5)
        self.zbuf[0] = self.pread.read()
        utime.sleep_us(5)
        self.zbuf[1] = self.pread.read()
        utime.sleep_us(5)
        self.zbuf[2] = self.pread.read()
        
        self.pos[2] = round((self.zbuf[0]+self.zbuf[1]+self.zbuf[2])/3)
        
        # If ADC reads less than high, or greater than low, touch detected:
        if 16 < self.pos[2] < 4080:
            self.tch = True
        else:
            self.tch = False
    
    # Method to Read All 3 Channels----------------------------------------||||
    def read(self):
        '''
        @brief Reads all 3 touch sensor channels in turn
        
        This method reads all three directions of the touch screen in 
        series. When no touch is detected, it will only read the 'z' direction 
        to save time. For an initial contact, it will proceed to read the 'x' 
        and 'y' directions. For subsequent readings with continuous contact, it 
        will read all 3 directions in an appropriate order to minimize reqired 
        pin transitions. 
        '''
        
        
        if self.tch == False:
            self.zread()
            if self.tch == True:
                self.xread()
                self.yread()
        else:
            if self.dset == 1:
                self.xread()
                self.zread()
                self.yread()
            else:
                self.yread()
                self.zread()
                self.xread()
            
            
            
        
# Test Program to Ensure Functionality------------------------------------->>>>   
if __name__ == '__main__':
    while True:
        try:
            test = input('\n'*20 + 'What direction would you like to test?\n     - For X direction, enter "x"\n     -For Y direction, enter "y"\n     -For contact test, enter "z"\n     -For full-system test, enter "Full"\n\n                    >>>     ')
            if test != 'x' and test != 'y' and test != 'z' and test != 'Full':
                test = 'Full'

            
            tch = TouchPanel('PA0','PA6','PA1','PA7',107,182,[2060, 2065])
            # dsp = touchDisp.touchDisp()
            
            if test == 'x':
                while True:
                    try:
                        t0 = utime.ticks_us()
                        tch.xread()
                        
                        t1 = utime.ticks_us()
                        tdif = utime.ticks_diff(t1,t0)
                        
                        print('\n'*20 +' You are touching at: X = ' + str(tch.pos[0]) + '\n')
                        print('Readtime: ' + str(tdif) + ' us' + '\n'*5)
                        utime.sleep_ms(100)
                    except KeyboardInterrupt:
                        break  
            elif test == 'y':
                while True:
                    try:
                        t0 = utime.ticks_us()
                        tch.yread()
                        
                        t1 = utime.ticks_us()
                        tdif = utime.ticks_diff(t1,t0)
                        
                        print('\n'*20 +' You are touching at: Y = ' + str(tch.pos[1]) + '\n')
                        print('Readtime: ' + str(tdif) + ' us' + '\n'*5)
                        utime.sleep_ms(100)
                    except KeyboardInterrupt:
                        break  
            elif test == 'z':
                while True:
                    try:
                        t0 = utime.ticks_us()
                        tch.zread()
                        if tch.tch == True:
                            t1 = utime.ticks_us()
                            tdif = utime.ticks_diff(t1,t0)
                            
                            print('\n' * 20 + 'You are touching the screen' + '\n')
                            print('Readtime: ' + str(tdif) + ' us' + '\n'*5)
                        else:
                            print('\n' * 20 + 'You are not touching the screen' + '\n'*7)
                        utime.sleep_ms(100)
                    except KeyboardInterrupt:
                        break  
            elif test == 'Full':
                while True:
                    try:
                        
                        if tch.tch == False:
                            t0 = utime.ticks_us()
                            tch.zread()
                            t1 = utime.ticks_us()
                            tdif = utime.ticks_diff(t1,t0)
                        if tch.tch == True:
                            t0 = utime.ticks_us()
                            tch.read()
                            t1 = utime.ticks_us()
                            tdif = utime.ticks_diff(t1,t0)
                            
                            print('\n'*20)
                            
                            # dsp.disp(tch)
                            
                            print('\n'*2 +' You are touching at: X = ' + str(tch.pos[0]) +'     Y = ' + str(tch.pos[1]) + '     Z = ' + str(tch.pos[2]) + ' \n')
                            print('Readtime: ' + str(tdif) + ' us' + '\n'*5)
                        else:
                            print('\n'*20)
                            # dsp.disp(tch)
                            print('\n' * 2 + 'You are not touching the screen' + '\n'*2)
                            print('Readtime: ' + str(tdif) + ' us' + '\n'*5)
                        utime.sleep_ms(100)
                    except KeyboardInterrupt:
                        break  
        except KeyboardInterrupt:
            print('\n'*50 + 'Sucessfuly Exited Test Program \n\n')
            break 