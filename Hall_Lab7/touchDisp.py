# -*- coding: utf-8 -*-
"""
Created on Sat Feb 27 22:10:03 2021

@author: hallm
"""

class touchDisp:
    
    def __init__(self):
        '''
        @brief bla bla bla
        @detail bla bla bla
        '''
        
        self.xpos = 0
        self.ypos = 0
        self.tch = False
        
        
    def disp(self,read):
        '''

        '''
        
        self.xpos = read.xpos
        self.ypos = read.ypos
        self.tch = read.tch
        
        if self.tch == True:
            T = 'True '
        else:
            T = 'False'
            
        X = ((self.xpos - 2048) / 4096)
        Xp = int(50*X) + 25
        
        Y = ((self.ypos - 2048) / 4096)
        Yp = int(10*Y) + 5
        
        noRow = ' '*51
        row = ' '*(Xp) + 'X' + ' '*(50 - Xp)
        
        store = [noRow,noRow,noRow,noRow,noRow,noRow,noRow,noRow,noRow,noRow,noRow]
        if self.tch == True:
            store[Yp] = row
        
        line = ' Touch = {a}' + ' '*5 + 'Xpos = {b:.2f}' + ' '*5 + 'Ypos = {c:.2f}'
        line = line.format(a=T,b=X,c=Y)
        line = line + ' '*(50 - len(line))
        
        scrn = '|---------------------------------------------------|\n' + '|' + store[0] + '|\n' + '|' + store[1] + '|\n' + '|' + store[2] + '|\n' + '|' + store[3] + '|\n' + '|' + store[4] + '|\n' + '|' + store[5] + '|\n' + '|' + store[6] + '|\n' + '|' + store[7] + '|\n' + '|' + store[8] + '|\n' + '|' + store[9] + '|\n' + '|' + store[10] + '|\n' + '|---------------------------------------------------|\n'
        print(scrn)