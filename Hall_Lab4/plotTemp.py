# -*- coding: utf-8 -*-
"""
Created on Thu Feb 11 07:42:32 2021

@author: hallm
"""

import array
import matplotlib
import numpy as np
from matplotlib import pyplot as plt
import csv



    # Plot voltage step response
with open ('HotOrNotCSV.csv', 'r') as tempCSV:
    csv_read = csv.reader(tempCSV, delimiter=',')
    line = 0
    time = []
    core = []
    ambn = []
    for row in csv_read:
        if line == 0:
            line = 1
        else:
            try:
                time.append(float(row[0])/3600)
                core.append(float(row[1]))
                ambn.append(float(row[2]))
            except:
                brk = min([len(time),len(core),len(ambn)]) - 1
                time = time[0:brk]
                core = core[0:brk]
                ambn = ambn[0:brk]
    
    plt.xlim([0,8])
    plt.ylim([-10,30])
    amb_temp, = plt.plot(time, ambn,'k', label='Ambient Temperature')
    cor_temp, = plt.plot(time, core,'grey', label='Core Temperature')
    plt.xlabel('Time [hr]')
    plt.ylabel('Temperature [C]')
    first_legend = plt.legend(handles=[cor_temp], loc='lower right')
    ax = plt.gca().add_artist(first_legend)
    plt.legend(handles=[amb_temp], loc='upper right')
    plt.savefig('Lab0x04_Temp_Data.jpg')