# -*- coding: utf-8 -*-
"""
@file ME405-Lab0x04-RHall-MHughes.py
@brief Main file for the Python temperature measuring device.
@details In this script, sensor data rom the MCP9808 temperature sensor is
recorded once per second over the course of a user-specified time duration,
in hours (defaults to 8 hr maximum). Upon running this script, the user will be 
prompted to enter a desired run time. The script will then inform the user that 
data is being collected, and includes a timer that will reduce approximately 
every 5 minutes. While the sensor is running, it will store readings in a 
buffer and periodically write the contents of the buffer to a CSV file. The 
user is informed when the collection is complete. At that point, the user may 
either press ctl+c to exit the program or restart the data collection. As the 
CSV is not erased between runs, the user should be cautious of repeated 
collection as the CSV may overflow. When the user exits the script, they will 
be notified of the total runtime of the data collection.

@author Rick Hall
@author Mason Hughes

@date Tue Feb  9 17:22:06 2021
"""

# Initialize & Verify Temp Sensor

import pyb
import os
from pyb import I2C
import utime
from MCP9808 import MCP9808
import array

adcall = pyb.ADCAll(12, 0x70000)
MCP = MCP9808(24)

time = 0
t0 = utime.ticks_ms()

prnt = False

timBuf = array.array('f',[])
corBuf = array.array('f',[])
ambBuf = array.array('f',[])

lpInx = 0

# Display Startup Message
rnTim = input('\n'*50 + 'Welcome to Tempotron! \n\n     Enter a desired run time in hours or press enter to run for 8 hrs:     >>>   ')

# Determine run time from
try:
    rnTim = int(rnTim)
    
    if rnTim <= 0 & rnTim >= 8:
        rnTimS = 60*60*rnTim
    else:
        rnTimS = 60*60*8
except:
    rnTimS = 60*60*8
    
print('\n'*50 + 'Sensor collecting data! \n\n     Check back in ' + str((rnTimS/3600)) + ' hours!')

# Before recording data, remove old data file
os.remove('HotOrNotCSV.csv')

if MCP.check() == True:
    with open ("HotOrNotCSV.csv", "w") as csvFile:
        csvFile.write('Time [s], Core Temperature [C], Ambient Temperature [C]\n')
        while True:
            try:
                # Read Core & Ambient Temperature
                core_temp = adcall.read_core_temp()
                amb_temp = MCP.celsius()
                
                # Get Current Time
                t1 = utime.ticks_ms()
                tdif = utime.ticks_diff(t1,t0)
                t0 = t1
                time = time + tdif
                
                # If Debug Print is True, Print Current Readings
                if prnt == True:
                    print('The CPU temperature is ' + str(core_temp) +' degrees at time: ' + str(time) + '\n')
                    print('The ambient temperature is ' + str(amb_temp) +' degrees at time: ' + str(time) +'\n')
                    
                # Wait before next collection
                utime.sleep(60)
                time_sec = time / 1000
                
                # Store data in RAM Buffer for 60 sec
                timBuf.append(time_sec)
                corBuf.append(core_temp)
                ambBuf.append(amb_temp)
                lpInx = lpInx + 1
                
                # Every minute, push Buffer to CSV file
                if lpInx == 4:
                    for inx in range(len(timBuf)):
                        line = "{a:.4f}, {b:.4f}, {c:.4f}\n"
                        line = line.format(a=timBuf[inx],b=corBuf[inx],c=ambBuf[inx])
                        csvFile.write(line)
                        
                    msg = 'Sensor now collecting data! \n\n     Check back in {d:.2f} hours!'
                    tmr = round(((rnTimS - time_sec)/ 3600),2)
                    print('\n'*50 + msg.format(d=tmr))
                    
                    timBuf = array.array('f',[])
                    corBuf = array.array('f',[])
                    ambBuf = array.array('f',[])
                    lpInx = 0
                
                if time_sec >= rnTimS:
                    print('\n'*50 + 'Data Collection Complete! \n\n')
                    break
                
                
            except KeyboardInterrupt:
                t_total = utime.ticks_diff(utime.ticks_ms(), t0)
                print('Data collected for ' + str(t_total/3600000) + ' hours ' + str(t_total/1000) + ' seconds')
                break