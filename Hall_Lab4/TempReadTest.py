# -*- coding: utf-8 -*-
"""
Created on Thu Feb  4 08:19:15 2021

@author: hallm

execfile('TempReadTest.py')
"""

import pyb
import utime
from MCP9808 import MCP9808

adcall = pyb.ADCAll(12, 0x70000)
MCP = MCP9808(24)

input("Welcome to TempOTron - Press ent to continue!")

time = 0
t0 = utime.ticks_ms()

with open ("TestCSV.csv", "w") as csvFile:
    csvFile.write('Time [s], Temperature [C]\n')
    lines = 1
    while True:
        try:
            temp = adcall.read_core_temp()
            amb_temp = MCP.celsius()
            
            t1 = utime.ticks_ms()
            tdif = utime.ticks_diff(t1,t0)
            t0 = t1
            time = time + tdif
            print('The CPU Temp is ' + str(amb_temp) +' degrees at time: ' + str(time))
            utime.sleep(1)
            time_sec = time / 1000
            
            line = str(time_sec) + ', ' + str(temp) + '\n'
            csvFile.write(line)
            lines = 2
            
            
        except KeyboardInterrupt:
            break
    

        