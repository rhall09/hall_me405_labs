# -*- coding: utf-8 -*-
"""
Created on Thu Feb  4 08:19:15 2021

@author: hallm
"""

import pyb
import utime

adcall = pyb.ADCAll(12)

input("Welcome to TempOTron - Press ent to continue!")

while True:
    try:
        temp = adcall.read_core_temp()
        print('The CPU Temp is ' + str(temp))
        utime.sleep_ms(1000)
    except KeyboardInterrupt():
        break
    

        