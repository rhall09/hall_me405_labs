# -*- coding: utf-8 -*-
"""
@file MCP9808.py
@brief Class to operate the MCP9808 temperature sensor through I2C communication.

@details This script contains a class to operate an MCP9808 temperature sensor
utilizing i2c communication. See class documentation for more details.

@package MCP9808
@brief This class operates the MCP9808 temperature sensor through I2C communication.

@details This script contains a class to operate an MCP9808 temperature sensor
utilizing i2c communication. See class documentation for more details.

@author Rick Hall
@author Mason Hughes

@date Tue Feb  9 22:03:52 2021
"""

from pyb import I2C
import utime



class MCP9808:
    '''
    @brief This class reads the MCP9808 Temperature Sensor
    
    @details This class operates the MCP9808 temperature sensor for the L476 Nucleo. 
    Using i2c communication protocols, this script contains methods to verify 
    sensor connection, read sensor in degrees celcius, and read sensor in degrees 
    fahrenheit. All computations in this script are based information available in 
    the documentation for this sensor hardware. 

    '''
    def __init__(self,address):
        '''
        @brief Initialization function for the MCP9808 class
        @details This function initializes the MCP9808 sensor, as well as i2c 
        communication on the pyboard.
        
        @param address The i2c address of the MCP9808 sensor
        '''
        
        self.i2c = I2C(1, I2C.MASTER)
        self.addr = address
        
        
        
        
    def check(self):
        '''
        @brief Method to test connection to MCP9808 sensor
        @details Reads the MCP9080 buffer corresponding to ... this is compared
        to the expected response based on the hardware documentation. If the 
        sensor is properly connected, it will read the correct value and this 
        method will return 'True.' If the sensor is not connected properly, it 
        will return 'False.'

        '''
        
        chkBuf = bytearray(2)
        
        self.i2c.mem_read(chkBuf, addr = self.addr, memaddr = 6)
        
        if chkBuf[1] == 0x0054:
            return True
        else:
            return False
        
        
    def celsius(self):
        '''
        @brief Method to read the sensor in Celsius
        @details This method pulls the reading from the appropriate buffer in
        the MCP9808 sensor. It then clears the flag bits in the high byte and
        combines the two bytes into a single decimal reading of the 
        temprature, with the decimal point located based on the MCP9808 
        documentation. When clearing the flag bits, the negative flag value 
        is temprarily stored to ensure the proper sign is used in the 
        conversion. 
        
        @return The temperature reading, in Celsius
        '''
        neg = 0
        
        # Retrieve array of 2 bytes from Temp sensor using i2c
        # memaddr is identified using the MCP9808 Documentation
        tempBuf = bytearray(2)
        self.i2c.mem_read(tempBuf, addr = self.addr, memaddr = 5)
        
        # Convert 2 byte data into usable temperature integer
        tempBuf[0] = tempBuf [0] & 0b00011111       # First 3 bits of MSB are 'flag' bits. As they are not used in this class, they are cleared
        if tempBuf[0] & 0b00010000 == 0b00010000:   # Fourth bite of MSB is sign flag - if raised, temp is negative
            tempBuf[0] = tempBuf[0] & 0b00001111
            neg = 256
        
        # Convert to Decimal by shifting both registers by 4 bits (mult by 16)
        tempC = int(tempBuf[0])*16 + int(tempBuf[1])/16 - neg        
            
        return tempC
        
    def fahrenheit(self):
        '''
        @brief Method to read the sensor in Fahrenheit
        @details This method is a simple conversion from Celsius to Fahrenheit; 
        it reads the temperature from the self.celsius() method, then converts 
        it accordingly. 

        @return The temperature reading, in Fahrenheit
        '''
        # Collect temperature data in Celsius, convert, and return
        tempC = self.celsius()
        tempF = tempC*(9/5) + 32
        
        return tempF
    
        
        

# Simple test script that prints temperature data to the console every second
if __name__ == '__main__':
    
    mcp = MCP9808(24)
    
    mcp.check()
    while True:
        try:
            print(mcp.celsius())
            print(mcp.fahrenheit())
            utime.sleep(1)
        except KeyboardInterrupt:
            break
        