'''
@file isrTest.py
@brief Simple script for testing of timer callbacks on the Nucleo.
@detail This script uses micropython to use the timer module on the Nucleo
to blink the the user LED.

@author Rick Hall

@date Mon Jan 25 12:37:45 2021

'''
pyb.enable_irq

import utime

LED = pyb.Pin(pyb.Pin.board.PA5, mode=pyb.Pin.OUT_PP)
tmr = pyb.Timer(2,prescaler=79, period=0x7fffffff)

Lb = 0
n = 0

def tock(line):
    
    global Lb
    global n
    
    n = n + 1
    
    if Lb == 0:
        LED.high()
        Lb = 1
        print("Turning LED ON!")
    elif Lb == 1:
        LED.low()
        Lb = 0
        print("Turning LED OFF!")
    else:
        print("ERROR")

tmr.callback(tock())

while True:
    utime.sleep_ms(500)
    print("Test File Running - Time: " + str(tmr.counter()))