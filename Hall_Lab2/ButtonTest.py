'''
@file ButtonTest.py
@brief Simple script for testing of button callbacks on the Nucleo.
@detail This script uses micropython to use the user input button on the Nucleo
as a toggle switch for the user LED.

@author Rick Hall

@date Mon Jan 25 09:39:46 2021

'''

pyb.enable_irq

LED = pyb.Pin(pyb.Pin.board.PA5, mode=pyb.Pin.OUT_PP)

Lb = 0

def ledIO(line):
    
    global Lb
    
    if Lb == 0:
        LED.high()
        Lb = 1
        print("LED ON!")
    elif Lb == 1:
        LED.low()
        Lb = 0
        print("LED OFF!")
    else:
        print("ERROR")
        
exint = pyb.ExtInt(pyb.Pin.board.PC13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, callback=ledIO)

while True:
    pass
