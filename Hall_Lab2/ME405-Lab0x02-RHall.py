'''
@file ME405-Lab0x02-RHall.py
@brief This script is a reaction time tester for a Nucleo microcontroller. 
@detail This script tests the user's reaction time by blinking an LED light and 
recording the delay between the LED activation and the press of a button. 
The system utilizes a rolling average to save RAM by limiting the number of bytes 
dedicated to storing data. If the user does not enter an input within 10 seconds, 
the system goes into an idle mode in which the LED will periodically blink twice 
in rapid succession and display an idle message. Idle will be exited when the button 
is pressed. To exit the program, the keyboard interrupt (ctl+c) may be used, 
at which time the rolling average will be displayed. If the user has not pressed 
the button at all, an error message will be displayed. 
\n \n
To run the script, copy and paste this line into your micropython terminal:
\n
execfile('ME405-Lab0x02-RHall.py')
\n

@author Rick Hall

@date Mon Jan 25 09:39:46 2021

execfile('ME405-Lab0x02-RHall.py')

'''
#----------Initialize Program--------------------------------------------------
# Import necessary modules
import utime
import random
import pyb
import micropython

# Enable Interupt Service Routines
pyb.enable_irq

# Create emergency buffer to store errors in ISR
micropython.alloc_emergency_exception_buf(200)

# Define Necessary Hardware Components
tmr = pyb.Timer(2,prescaler=79, period=0x7fffffff)
led = pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)
btn = pyb.Pin(pyb.Pin.cpu.C13, mode=pyb.Pin.IN)

# Initialize Necessary Variables
React_Time = 0          # Reaction Time, in us
Roll_Average = 0        # Rolling Average Reaction Time, in us
sec = 0                 # Average Reaction Time, in seconds
Idle = 0                # Idle State Boolean
Test_Start = 0          # Test Status Boolean
Button_Pressed = 0      # Button Pressed Boolean
L = 0                   # LED State Boolean
n = 0                   # Number of Test Attempts


def React(line):
    '''
    @brief Button press ISR to collect time of reaction to the milisecond

    '''
    global React_Time
    global Test_Start
    global Button_Pressed
    global L
    global React_Time
    
    if Test_Start == 1:
        React_Time = tmr.counter()
        Test_Start = 0
        Button_Pressed = 1
        # print("You have pressed THE BUTTON!")
        if L == 1:
            led.low()
            L = 0
        
    else:
        print("Wait for LED!")
    
exint = pyb.ExtInt(btn, 
                   pyb.ExtInt.IRQ_FALLING, 
                   pyb.Pin.PULL_UP, 
                   callback=React)

# Print welcome message and directions
print("Welcome to Reactotron!"+"\n"+"We're going to measure you're reaction time!" + "\n"*2)
utime.sleep_ms(3000)
print("When the green light turns on, press the blue button!" + '\n'*2)
utime.sleep_ms(3000)
print("The test starts now! Are you ready?" + '\n'*3)

#<<<<<-----Run Reaction Time Test----------------------------------------->>>>>
while True:
    #-----Continue Test Until Keyboard Interrupt-------------------------------
    try:
        #-----Run while user continues to input--------------------------------
        if Idle == 0:
            
            # Delay 2 seconds, plus a random delay up to one second
            t = 2000 + random.randint(1,1000)
            utime.sleep_ms(t)
            
            # Turn LED ON and reset timer

            led.high()
            tmr.counter(0)
            L = 1
            Test_Start = 1
            # print("LED turning on!")

            # Wait 1 second, then turn off LED
            utime.sleep_ms(1000)
            led.low()
            L = 0
            # print("LED turning of!")
           
            # Spin until either user inputs or system enters idle
            while Button_Pressed == 0 and Idle == 0:
                utime.sleep_ms(100)
                if tmr.counter() >= 10E6:
                    Idle = 1
            # So long as system is NOT entering idle, run computations
            if Idle == 0:
                n = n + 1
                if Roll_Average == 0:
                    # If first loop, set average to value
                    Roll_Average = React_Time
                else:
                    # Rolling Average Formula
                    Roll_Average  = Roll_Average + (1/n)*(React_Time-Roll_Average)
                
                sec = round(Roll_Average/1E6,6)
                
                # Print Reaction Time for User
                print("You reacted in " +str(round(React_Time/1E6,6))+ " seconds")    
                # print("Your averate is: " + str(sec) + " seconds")

            Button_Pressed = 0
        
        #-----Idle if no input-------------------------------------------------       
        else:  
            # Run idle until user presses button
            if Button_Pressed == 0:
                print("System Idling - Press Button to Continue")
                led.high()
                utime.sleep_ms(100)
                led.low()
                utime.sleep_ms(50)
                led.high()
                utime.sleep_ms(100)
                led.low()
                utime.sleep_ms(2750)
            
            # Reset system before resuming    
            elif Button_Pressed == 1:
                Idle = 0
                Test_Start = 0
                Button_Pressed = 0
                tmr.counter(0)
              
    #-----Stop Test Upon Keyboard Interrupt------------------------------------            
    except KeyboardInterrupt:
        
        # Display average reaction time for User
        if n > 1:
            print('\n'*3)
            print('Your average reaction time is: ' + str(sec) + ' seconds over ' + str(n) + ' trys!')
            print('\n'*3)
        
        # Display error message if no user inputs recorded
        elif n == 1:
            print('\n'*3)
            print("Your SINGLE reaction time is: " + str(sec) + ' seconds!')
            print('\n'*3)
        elif n == 0:
            print('\n'*3)
            print("Um... did you not see the LED?")
            print('\n'*3)
            
        utime.sleep_ms(3000)
        
        break
    
    
    
    
    


    
